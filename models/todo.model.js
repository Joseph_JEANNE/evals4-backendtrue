var PostgresStore = require('../utils/PostgresStore.js');

class Todo {
  static toSqlTable () {
    return `
        CREATE TABLE todo (
            id SERIAL PRIMARY KEY,
            content VARCHAR(145)
        )
        `;
  }

  static async insert (content) {
    const result = await PostgresStore.pool.query({
      text: `
            INSERT INTO $(Todo.tableName)
            (content)
            VALUES ($1) RETURNING *
            `,
      values: [content]
    });
    return result;
  }

  static async delete (content) {
    const result = await PostgresStore.pool.query({
      text: `
            DELETE FROM $(Todo.tableName)
            WHERE
            content = $1 RETURNING *
            `,
      values: [content]
    });
    return result;
  }

  static async getAll () {
    const result = await PostgresStore.client.query({
      text: `
            SELECT * FROM $(Todo.tableName)
            `
    });
    return result;
  }
}
/** @type {String} */
Todo.tableName = 'todo';

module.exports = Todo
;

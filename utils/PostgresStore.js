const pg = require('pg');
const pgtools = require('pgtools');
const config = require('../server.config.js');
const todo = require('../models/todo.model.js');

class PostgresStore {
  async init () {
    this.pool = new pg.Pool(config.postgres);
  }

  close () {
    if (this.pool) this.pool.close();
    this.pool = null;
  }

  async reset () {
    const conf = {
      user: config.postgres.user,
      host: config.postgres.host,
      password: config.postgres.password
    };
    try {
      await pgtools.dropdb(conf, config.postgres.database);
    } catch (err) {
      console.log('Pas niquel', err);
    }
    await pgtools.createdb(conf, config.postgres.database);
    await this.init();
    await this.buildTables();
    console.log('Niquel !');
  }

  async buildTables () {
    const q = todo.toSqlTable();
    if (Array.isArray(q)) {
      for (const query of q) {
        console.log(query);
        await this.pool.query(query);
      }
    } else {
      console.log(q);
      await this.pool.query(q);
    }
  }
}
module.exports = new PostgresStore()
;

var express = require('express');
var router = express.Router();
var Todo = require('../models/todo.model.js');

console.log('Dans index !');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/render', async function (req, res, next) {
  const todos = await Todo.getAll();
  res.json(todos.rows);
});

router.get('/add', async function (req, res, next) {
  const addTodo = await Todo.insert(req.query.content);
  res.json(addTodo.rows);
});

router.get('/delete', async function (req, res, next) {
  const delTodo = await Todo.delete(req.query.content);
  res.json(delTodo.rows);
});

module.exports = router;
